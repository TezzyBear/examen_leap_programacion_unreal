// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProyectPregunta4/ProyectPregunta4GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProyectPregunta4GameModeBase() {}
// Cross Module References
	PROYECTPREGUNTA4_API UClass* Z_Construct_UClass_AProyectPregunta4GameModeBase_NoRegister();
	PROYECTPREGUNTA4_API UClass* Z_Construct_UClass_AProyectPregunta4GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_ProyectPregunta4();
// End Cross Module References
	void AProyectPregunta4GameModeBase::StaticRegisterNativesAProyectPregunta4GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AProyectPregunta4GameModeBase_NoRegister()
	{
		return AProyectPregunta4GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ProyectPregunta4,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ProyectPregunta4GameModeBase.h" },
		{ "ModuleRelativePath", "ProyectPregunta4GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AProyectPregunta4GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::ClassParams = {
		&AProyectPregunta4GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AProyectPregunta4GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AProyectPregunta4GameModeBase, 1143420088);
	template<> PROYECTPREGUNTA4_API UClass* StaticClass<AProyectPregunta4GameModeBase>()
	{
		return AProyectPregunta4GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProyectPregunta4GameModeBase(Z_Construct_UClass_AProyectPregunta4GameModeBase, &AProyectPregunta4GameModeBase::StaticClass, TEXT("/Script/ProyectPregunta4"), TEXT("AProyectPregunta4GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProyectPregunta4GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
