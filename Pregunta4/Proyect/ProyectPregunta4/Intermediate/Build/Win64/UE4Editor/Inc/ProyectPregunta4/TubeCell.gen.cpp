// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProyectPregunta4/TubeCell.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTubeCell() {}
// Cross Module References
	PROYECTPREGUNTA4_API UClass* Z_Construct_UClass_ATubeCell_NoRegister();
	PROYECTPREGUNTA4_API UClass* Z_Construct_UClass_ATubeCell();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_ProyectPregunta4();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ATubeCell::execOnSelected)
	{
		P_GET_OBJECT(AActor,Z_Param_Target);
		P_GET_STRUCT(FKey,Z_Param_ButtonPressed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnSelected(Z_Param_Target,Z_Param_ButtonPressed);
		P_NATIVE_END;
	}
	void ATubeCell::StaticRegisterNativesATubeCell()
	{
		UClass* Class = ATubeCell::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnSelected", &ATubeCell::execOnSelected },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATubeCell_OnSelected_Statics
	{
		struct TubeCell_eventOnSelected_Parms
		{
			AActor* Target;
			FKey ButtonPressed;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ButtonPressed;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATubeCell_OnSelected_Statics::NewProp_ButtonPressed = { "ButtonPressed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TubeCell_eventOnSelected_Parms, ButtonPressed), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATubeCell_OnSelected_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TubeCell_eventOnSelected_Parms, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATubeCell_OnSelected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATubeCell_OnSelected_Statics::NewProp_ButtonPressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATubeCell_OnSelected_Statics::NewProp_Target,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATubeCell_OnSelected_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TubeCell.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATubeCell_OnSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATubeCell, nullptr, "OnSelected", nullptr, nullptr, sizeof(TubeCell_eventOnSelected_Parms), Z_Construct_UFunction_ATubeCell_OnSelected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATubeCell_OnSelected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATubeCell_OnSelected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATubeCell_OnSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATubeCell_OnSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATubeCell_OnSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATubeCell_NoRegister()
	{
		return ATubeCell::StaticClass();
	}
	struct Z_Construct_UClass_ATubeCell_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATubeCell_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_ProyectPregunta4,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATubeCell_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATubeCell_OnSelected, "OnSelected" }, // 1576640443
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATubeCell_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "TubeCell.h" },
		{ "ModuleRelativePath", "TubeCell.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATubeCell_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATubeCell>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATubeCell_Statics::ClassParams = {
		&ATubeCell::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATubeCell_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATubeCell_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATubeCell()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATubeCell_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATubeCell, 1023256251);
	template<> PROYECTPREGUNTA4_API UClass* StaticClass<ATubeCell>()
	{
		return ATubeCell::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATubeCell(Z_Construct_UClass_ATubeCell, &ATubeCell::StaticClass, TEXT("/Script/ProyectPregunta4"), TEXT("ATubeCell"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATubeCell);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
