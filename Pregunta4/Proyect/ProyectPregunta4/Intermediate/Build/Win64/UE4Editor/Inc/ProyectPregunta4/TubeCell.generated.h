// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FKey;
#ifdef PROYECTPREGUNTA4_TubeCell_generated_h
#error "TubeCell.generated.h already included, missing '#pragma once' in TubeCell.h"
#endif
#define PROYECTPREGUNTA4_TubeCell_generated_h

#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_SPARSE_DATA
#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnSelected);


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnSelected);


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATubeCell(); \
	friend struct Z_Construct_UClass_ATubeCell_Statics; \
public: \
	DECLARE_CLASS(ATubeCell, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProyectPregunta4"), NO_API) \
	DECLARE_SERIALIZER(ATubeCell)


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATubeCell(); \
	friend struct Z_Construct_UClass_ATubeCell_Statics; \
public: \
	DECLARE_CLASS(ATubeCell, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProyectPregunta4"), NO_API) \
	DECLARE_SERIALIZER(ATubeCell)


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATubeCell(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATubeCell) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATubeCell); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATubeCell); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATubeCell(ATubeCell&&); \
	NO_API ATubeCell(const ATubeCell&); \
public:


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATubeCell(ATubeCell&&); \
	NO_API ATubeCell(const ATubeCell&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATubeCell); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATubeCell); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATubeCell)


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_PRIVATE_PROPERTY_OFFSET
#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_9_PROLOG
#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_PRIVATE_PROPERTY_OFFSET \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_SPARSE_DATA \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_RPC_WRAPPERS \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_INCLASS \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_PRIVATE_PROPERTY_OFFSET \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_SPARSE_DATA \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_INCLASS_NO_PURE_DECLS \
	ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROYECTPREGUNTA4_API UClass* StaticClass<class ATubeCell>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProyectPregunta4_Source_ProyectPregunta4_TubeCell_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
