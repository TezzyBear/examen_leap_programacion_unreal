// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROYECTPREGUNTA4_ProyectPregunta4GameModeBase_generated_h
#error "ProyectPregunta4GameModeBase.generated.h already included, missing '#pragma once' in ProyectPregunta4GameModeBase.h"
#endif
#define PROYECTPREGUNTA4_ProyectPregunta4GameModeBase_generated_h

#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_SPARSE_DATA
#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_RPC_WRAPPERS
#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProyectPregunta4GameModeBase(); \
	friend struct Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProyectPregunta4GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProyectPregunta4"), NO_API) \
	DECLARE_SERIALIZER(AProyectPregunta4GameModeBase)


#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProyectPregunta4GameModeBase(); \
	friend struct Z_Construct_UClass_AProyectPregunta4GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProyectPregunta4GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProyectPregunta4"), NO_API) \
	DECLARE_SERIALIZER(AProyectPregunta4GameModeBase)


#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProyectPregunta4GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProyectPregunta4GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProyectPregunta4GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProyectPregunta4GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProyectPregunta4GameModeBase(AProyectPregunta4GameModeBase&&); \
	NO_API AProyectPregunta4GameModeBase(const AProyectPregunta4GameModeBase&); \
public:


#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProyectPregunta4GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProyectPregunta4GameModeBase(AProyectPregunta4GameModeBase&&); \
	NO_API AProyectPregunta4GameModeBase(const AProyectPregunta4GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProyectPregunta4GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProyectPregunta4GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProyectPregunta4GameModeBase)


#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_12_PROLOG
#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_SPARSE_DATA \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_RPC_WRAPPERS \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_INCLASS \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_SPARSE_DATA \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROYECTPREGUNTA4_API UClass* StaticClass<class AProyectPregunta4GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProyectPregunta4_Source_ProyectPregunta4_ProyectPregunta4GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
