// Fill out your copyright notice in the Description page of Project Settings.


#include "TubeCell.h"

// Sets default values
ATubeCell::ATubeCell()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	OnClicked.AddUniqueDynamic(this, &ATubeCell::OnSelected);
}

// Called when the game starts or when spawned
void ATubeCell::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATubeCell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATubeCell::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATubeCell::OnSelected(AActor* Target, FKey ButtonPressed)
{
	UE_LOG(LogTemp, Warning, TEXT("The integer value is: 999"));
}

