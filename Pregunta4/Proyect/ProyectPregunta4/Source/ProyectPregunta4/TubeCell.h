// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TubeCell.generated.h"

UCLASS()
class PROYECTPREGUNTA4_API ATubeCell : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATubeCell();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void OnSelected(AActor* Target, FKey ButtonPressed);

};
