// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Board.generated.h"

class ATubeCell;

UCLASS()
class PROYECTPREGUNTA4_API ABoard : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ABoard();

	enum CELL_TYPE { TUBE, CURVE, POINT };

	int matrixSize = 4;
	float rotationMatrix[4][4] = {
		{  0, 90,  0,  0},
		{180,  0,  0,  0},
		{ 90,  0,  0,  0},
		{180,  0,  0,  0},
	};
	CELL_TYPE typeMatrix[4][4] = {
		{CELL_TYPE::POINT, CELL_TYPE::POINT, CELL_TYPE::POINT,CELL_TYPE::POINT},
		{CELL_TYPE::POINT, CELL_TYPE::CURVE, CELL_TYPE::CURVE,CELL_TYPE::POINT},
		{CELL_TYPE::POINT, CELL_TYPE::TUBE, CELL_TYPE::POINT,CELL_TYPE::POINT},
		{CELL_TYPE::POINT, CELL_TYPE::CURVE, CELL_TYPE::POINT,CELL_TYPE::POINT}
	};

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATubeCell> tubeCellClass;

	UPROPERTY(EditAnywhere)
		class UMaterial* tubeMaterial;
	UPROPERTY(EditAnywhere)
		class UMaterial* curveMaterial;
	UPROPERTY(EditAnywhere)
		class UMaterial* pointMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
