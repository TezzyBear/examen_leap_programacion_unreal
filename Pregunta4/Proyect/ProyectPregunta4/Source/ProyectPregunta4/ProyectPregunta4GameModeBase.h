// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProyectPregunta4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROYECTPREGUNTA4_API AProyectPregunta4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
