// Fill out your copyright notice in the Description page of Project Settings.


#include "Board.h"
#include "TubeCell.h"

// Sets default values
ABoard::ABoard()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABoard::BeginPlay()
{
	Super::BeginPlay();

	UWorld* world = GetWorld();

	if (world) {
		for (int i = 0; i < matrixSize; i++) {
			for (int j = 0; j < matrixSize; j++) {

				FVector spawnLocation = GetTransform().GetLocation();
				spawnLocation.X += (j * 105) - 157.5;
				spawnLocation.Y += (i * 105) - 157.5;

				FTransform spawnTransform = GetTransform();
				spawnTransform.SetLocation(spawnLocation);

				ATubeCell* inst = world->SpawnActor<ATubeCell>(tubeCellClass, spawnTransform);

				TArray<UStaticMeshComponent*> StaticComps;
				inst->GetComponents<UStaticMeshComponent>(StaticComps);

				if (StaticComps.Num()) { // Validating that the array isnt empty
					switch (typeMatrix[i][j]) {
					case(CELL_TYPE::TUBE):
						StaticComps[0]->SetMaterial(0, tubeMaterial);
						break;
					case(CELL_TYPE::CURVE):
						StaticComps[0]->SetMaterial(0, curveMaterial);
						break;
					case(CELL_TYPE::POINT):
						StaticComps[0]->SetMaterial(0, pointMaterial);
						break;
					}					
				}				

				inst->SetActorRotation(FRotator(0.f, rotationMatrix[i][j], 0.f));			

				UE_LOG(LogTemp, Warning, TEXT("The integer value is: %d"), rotationMatrix[i][j]);
			}
		}
	}
}

// Called every frame
void ABoard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

