#include <iostream>
#include <math.h>
#include <SFML/Graphics.hpp>
#include <vector>

#define PI 3.14159265

int main()
{
    srand(time(NULL));

    int n; //number of points
    float r; //radius
    float x, y; //x and y position of center

    std::cout << "Ingrese el numero de circulos (n): "; 
    std::cin >> n;
    std::cout << "Ingrese el valor del radio (r): ";
    std::cin >> r;

    std::cout << "Ingrese el valor x del centro P(x,y): ";
    std::cin >> x;
    std::cout << "Ingrese el valor y del centro P(x,y): ";
    std::cin >> y;

    float circleSize = 3.f; //size of each point used to build semi-circle
    float PosOffset = 250 - circleSize; //offset so 0,0 isnt top left of window
    std::vector<sf::CircleShape*> circleVec; //vector to store pointers to circles

    float incrementVal = ((180 * PI / 180) + (180 * PI / 180) / n) / n ; //calculation of increment for i position

    for (float i = 0; i <= 180*PI/180; i+=incrementVal) {
        sf::CircleShape* circle = new sf::CircleShape(circleSize);
        circle->setPosition(x+cos(i)*r + PosOffset, y+sin(i)*r + PosOffset);
        circle->setFillColor(sf::Color(255, rand() % 128, rand() % 128, 255));
        circleVec.push_back(circle);
        std::cout << "Numero de circulos: " << circleVec.size() << std::endl;
    }

    sf::RenderWindow window(sf::VideoMode(500, 500), "Pregunta 3"); //window creation with SFML

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();                
                circleVec.clear();
                circleVec.empty();
            }                
        }

        window.clear();

        for (int i = 0; i < circleVec.size(); i++) {
            window.draw(*circleVec[i]);
        }

        window.display();
    }

    return 0;
}