#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>

int N = 10;
int n = 0;
int R = 70; // 70
float T = 15; //in seconds

void Print(std::string text) {
	std::cout << text << std::endl;
}

struct timer
{
	typedef std::chrono::steady_clock clock;
	typedef std::chrono::seconds seconds;

	void reset() { start = clock::now(); }

	unsigned long long seconds_elapsed() const
	{
		return std::chrono::duration_cast<seconds>(clock::now() - start).count();
	}

private: clock::time_point start = clock::now();
};

struct EnemyAttack {
	float attackDuration;
	std::vector<std::string> attackTags;
	std::vector<std::string> reactionTags;
	EnemyAttack() {}
	EnemyAttack(float attackDuration, std::vector<std::string> attackTags, std::vector<std::string> reactionTags) :
		attackDuration(attackDuration), attackTags(attackTags), reactionTags(reactionTags)
	{}
};

class Enemy {
private:
	std::string enemyName;
	std::string enemyType;
	std::vector<EnemyAttack> enemyAttacks;
public:
	Enemy(std::string enemyName, std::string enemyType) :
		enemyName(enemyName), enemyType(enemyType), enemyAttacks()
	{}
	void AddEnemyAttack(EnemyAttack enemyAttack) {
		enemyAttacks.push_back(enemyAttack);
	}
	void Attack(int id) {
		Print("El enemigo " + enemyName + " ha intentado atacar.");
		n++;
		EnemyAttack enemyAttack = enemyAttacks[id];		
	}
	std::string GetEnemyName() {
		return enemyName;
	}
	std::string GetEnemyType() {
		return enemyType;
	}
	std::vector<EnemyAttack> GetEnemyAttacks() {
		return enemyAttacks;
	}
};

class DirectorDeOrquestra
{
private:
	std::vector<std::string>blockedTypes;
	std::vector<Enemy> activeEnemies;
	EnemyAttack postedAttack;
	timer t;
	std::vector<timer> blockedTimers;
	std::vector<int> blockedTimes;
	bool tFlag;
	bool nFlag;
	DirectorDeOrquestra() {
		t.reset();
		tFlag = false;
		nFlag = false;
	}
public:
	static DirectorDeOrquestra& GetInstance() {
		static DirectorDeOrquestra instance;
		return instance;
	}
	void UpdateEnemies(std::vector<Enemy> enemies) {
		activeEnemies = enemies;
	}
	void CreateSignal(Enemy enemy, std::string enemyType) {
		for (int i = 0; i < blockedTypes.size(); i++) {
			if (blockedTimers[i].seconds_elapsed() > blockedTimes[i]) {
				Print("[ORQUESTRA-MSG]: La ventana de tiempo de bloqueo de ataques de tipo " + enemyType + " ha expirado.");
				blockedTypes.erase(blockedTypes.begin() + i);
				blockedTimers.erase(blockedTimers.begin() + i);
				blockedTimes.erase(blockedTimes.begin() + i);
				//falta agregar push backear los elementos de blocked times y d eblocked timers
			}		
		}
		if (t.seconds_elapsed() > T) {
			Print("[ORQUESTRA-MSG]: La ventana de tiempo de bloqueo de ataque en orquesta ha terminado.");
			tFlag = true;
		}
		if (n > N) {
			Print("[ORQUESTRA-MSG]: El n�mero m�nimo de ataques simples para desbloquear un ataque orquesta ha sido alcanzado. (n = " + std::to_string(n) + " )");
			nFlag = true;
		}
		if (nFlag && tFlag) {
			if (rand() % 100 > R) {
				Enemy* otherEnemy = SearchEnemy();
				if (otherEnemy) {
					otherEnemy->Attack(0);
					n = 0;
					t.reset();
					Print("[ORQUESTRA-FDB]: El sistema ha originado un ataque de orquesta entre enemigo " + enemy.GetEnemyName() + " y " + otherEnemy->GetEnemyName() + ".");
				}
			}
		}
		std::string blockedTypesStr;
		for (int i = 0; i < blockedTypes.size(); i++) {
			blockedTypesStr += blockedTypes[i] + ", ";
			if (blockedTypes[i] == enemyType) {
				Print("[ORQUESTRA-FDB]: El enemigo no puede atacar porque los ataques de tipo: " + blockedTypesStr.substr(0, blockedTypesStr.size() - 2) + " han sido bloqueados.");
				Print("[ORQUESTRA-FDB]: Tiempo restante: " + std::to_string(blockedTimes[i] - blockedTimers[i].seconds_elapsed()) + ".");
				break;
			}
		}

		if (std::find(blockedTypes.begin(), blockedTypes.end(), enemyType) != blockedTypes.end()) {
		
		}
		else {
			blockedTypes.push_back(enemyType);
			blockedTimers.push_back(timer());
			blockedTimes.push_back(enemy.GetEnemyAttacks()[0].attackDuration);
		}
		
		postedAttack = enemy.GetEnemyAttacks()[0];
		
			//initialize timer basado en enemyAttack timer
	}
	Enemy* SearchEnemy() {

		for (int i = 0; i < postedAttack.attackTags.size(); i++) {
			for (int j = 0; j < activeEnemies.size(); j++) {
				auto eReactionTags = activeEnemies[j].GetEnemyAttacks()[0].reactionTags;
				bool found = (std::find(eReactionTags.begin(), eReactionTags.end(), postedAttack.attackTags[i]) != eReactionTags.end()); //contains posted attack tag
				if (found)
					return &(activeEnemies[j]);
			}
		}
		return nullptr;					
	}
};

int main() {

	srand(time(NULL));

	std::vector<Enemy> activeEnemies;

	Enemy enemyRana("Rana", "Terrestre");
	enemyRana.AddEnemyAttack(EnemyAttack(
		5.f,
		{"flincher", "knockdown"},
		{"bullet"}
	));
	activeEnemies.push_back(enemyRana);

	Enemy enemyTronco("Tronco", "Terrestre");
	enemyTronco.AddEnemyAttack(EnemyAttack(
		8.f,
		{ "charger", "distance" },
		{ "knockdown" }
	));
	activeEnemies.push_back(enemyTronco);

	Enemy enemyMono("Mono", "Aereo");
	enemyMono.AddEnemyAttack(EnemyAttack(
		2.f,
		{ "distance", "bullet" },
		{ "charger" }
	));
	activeEnemies.push_back(enemyMono);

	DirectorDeOrquestra::GetInstance().UpdateEnemies(activeEnemies);

	timer t;
	while (true) {

		Print("\n---------------------");
		printf("Tiempo pasado: %llu", t.seconds_elapsed());
		std::cout << std::endl;

		char symbol;
		Print("Ingresa una tecla y presiona enter:");
		std::cin >> symbol;
		std::cout << std::endl;

		switch (symbol)
		{
		case 'q': //Rana
			enemyRana.Attack(0);
			DirectorDeOrquestra::GetInstance().CreateSignal(enemyRana, enemyRana.GetEnemyType());
			break;
		case 'w': //Tronco
			enemyTronco.Attack(0);
			DirectorDeOrquestra::GetInstance().CreateSignal(enemyTronco, enemyTronco.GetEnemyType());
			break; 
		case 't': //Mono
			enemyMono.Attack(0);
			DirectorDeOrquestra::GetInstance().CreateSignal(enemyMono, enemyMono.GetEnemyType());
			break;
		default:
			break;
		}
	}


	std::cout << "Fin del juego!" << std::endl;
	std::cin.get();
}