#include <iostream>
#include <vector>
#include <cmath>

# define PI 3.14159265358979323846



struct Vec3 {
	float x, y, z;
	Vec3() {}
	Vec3(float x, float y, float z):
		x(x), y(y), z(z) 
	{
	
	}
	Vec3 operator+(const Vec3& other) {
		return Vec3(x + other.x, y + other.y, z + other.z);
	}
	Vec3 operator-(const Vec3& other) {
		return Vec3(x - other.x, y - other.y, z - other.z);
	}
};

float dot(Vec3 a, Vec3 b)  //calculates dot product of a and b
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

float mag(Vec3 a)  //calculates magnitude of a
{
	return std::sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

float getAngleBetween(Vec3 vec, Vec3 other) {

	return std::acos(dot(vec, other) / (mag(vec) * mag(other))) * 180 / PI;
}

float getDistanceBetween(Vec3 vec, Vec3 other) {
	return sqrt(pow(other.x - vec.x, 2) + pow(other.y - vec.y, 2) + pow(other.z - vec.z, 2));
}

float getDistToLine(Vec3 linePoint, Vec3 lineDir, Vec3 otherPoint) {
	float b = getDistanceBetween(linePoint, otherPoint);
	Vec3 enemyVec = otherPoint - linePoint;
	float ang = getAngleBetween(lineDir, enemyVec);
	return b * sin(ang);
}

class Enemy {
public:
	Vec3 pos; //world position
	bool isRendered;
public:
	Enemy(Vec3 pos) :
		pos(pos), isRendered(false)
	{
		std::cout << "Se creo un enemigo en la posicion: (" <<
			pos.x << ", " << pos.y << ", " << pos.z << ")" << std::endl;
	}
};

class Player {
private:
	Vec3 pos; //world position
	Vec3 pForward; //forward direction
public:
	Player(Vec3 pos, Vec3 dir) :
		pos(pos), pForward(dir)
	{}
	Enemy* lockOnEnemy(const std::vector<Enemy*> enemies) {
		//selecting enemies in a 45 degree cone closer to a distance of 20
		std::vector<Enemy*> enemiesInCone;
		for (int i = 0; i < enemies.size(); i++) {
			if (getDistanceBetween(pos, enemies[i]->pos) > 20)
				continue;
			else if (getAngleBetween(pForward, enemies[i]->pos - pos) < 45) {
				enemiesInCone.push_back(enemies[i]);
			}
		}
		//chosing enemy closest to forward vector
		float closestDistance = 999999;
		Enemy* closestElegibleEnemy = nullptr;
		for (int i = 0; i < enemiesInCone.size(); i++) {
			float distToLine = getDistToLine(pos, pForward, enemiesInCone[i]->pos);
			if (distToLine < closestDistance) {
				closestDistance = distToLine;
				closestElegibleEnemy = enemiesInCone[i];
			}
		}

		if (closestElegibleEnemy == nullptr) {
			closestElegibleEnemy = enemies[rand() % enemies.size()];
		}
		return closestElegibleEnemy;
	}
};

int main() {

	srand(time(NULL));

	Player player(Vec3(0.f, 0.f, 0.f), Vec3(1.f, 0.f, 0.f));

	int nEnemies = 10;
	std::vector<Enemy*> enemyVec;

	//assuming a world size of 500 units
	for (int i = 0; i < nEnemies; i++) {
		float randX = rand() % 501;
		float randY = rand() % 501;
		float randZ = rand() % 501;
		enemyVec.push_back(new Enemy(Vec3(randX, randY, randZ)));
	}

	enemyVec.push_back(new Enemy(Vec3(1.0f, 0.f, 0.f)));

	Vec3 pPos, pDir, ePos;

	std::cout << player.lockOnEnemy(enemyVec)->pos.x << std::endl;

	std::cin.get();

}