#include <iostream>
#include <string>

struct Rect {
	static int count;
	float x, y;
	float bbox_left, bbox_top, bbox_right, bbox_bottom;
	float width, height;
	Rect():
		x(0.f), y(0.f), width(0.f), height(0.f) 
	{
		count++;

		bbox_left = x - (width / 2);
		bbox_top = y + (height / 2);
		bbox_right = x + (width / 2);
		bbox_bottom = y - (height / 2);

		//std::cout << "Has creado un rectanglo en la posicion: " << x << ", " << y << std::endl;
	}
	Rect(float x, float y, float width, float height) :
		x(x), y(y), width(width), height(height)
	{
		bbox_left = x - (width / 2);
		bbox_top = y + (height / 2);
		bbox_right = x + (width / 2);
		bbox_bottom = y - (height / 2);

		count++;
		std::cout << "Has creado un rectanglo en la posicion: " << x << ", " << y << std::endl;
	}
	bool collidesWithRect(const Rect& rect) {		

		if (bbox_left <= rect.bbox_right && bbox_right >= rect.bbox_left 
			&& bbox_top >= rect.bbox_bottom && bbox_bottom <= rect.bbox_top) {
			return true;		
		}		
		return false;
	}
	std::string  getRectStr() {
		std::string rectStr = "(";
		rectStr.append(std::to_string(x));
		rectStr.append(", ");
		rectStr.append(std::to_string(y));
		rectStr.append("); width: ");
		rectStr.append(std::to_string(width));
		rectStr.append(", height: ");
		rectStr.append(std::to_string(height));
		rectStr.append(")");
		return rectStr;
	}
};

int Rect::count = 0;

int main() {

	std::cout << "Pregunta 2!" << std::endl;

	Rect rectArray[3];

	rectArray[0] = Rect(1.f, 0.f, 1.f, 1.f);
	rectArray[1] = Rect(2.f, 1.f, 1.f, 1.f);
	rectArray[2] = Rect(-1.f, 1.f, 1.f, 1.f);

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			if (i != j) {				
				std::cout << std::endl;
				bool res = rectArray[i].collidesWithRect(rectArray[j]);
				std::string resStr = res == true ? "si" : "no";
				std::cout << "El rectangulo " << rectArray[i].getRectStr() << " " << resStr + " colisiona con el rectangulo " << rectArray[j].getRectStr() << std::endl;
			}
		}
	}

	std::cin.get();
	
}